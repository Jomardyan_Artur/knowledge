let jest = {
    condition: {},
    fn: function decorator(callback) {
        return function c() {
            ++jest.condition[c] || (jest.condition[c] = 1);
            callback();
        };
    }
};

const mockFunc = jest.fn(() => console.log("Called!"));

mockFunc();
mockFunc();

function expect(func) {
    return {
        toBeCalledTime: (number) => {
            console.log(number === jest.condition[func]);
        }
    };
}

expect(mockFunc).toBeCalledTime(2);